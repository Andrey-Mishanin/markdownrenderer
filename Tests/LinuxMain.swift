import XCTest

import MarkdownRendererTests

var tests = [XCTestCaseEntry]()
tests += MarkdownRendererTests.allTests()
XCTMain(tests)