//
//  MarkdownParserCoreTests.swift
//  MarkdownParserCoreTests
//
//  Created by Andrey Mishanin on 12/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import XCTest
import MarkdownRendererCore

final class ParserTests_Headings: XCTestCase {
  func test_ParsingHeading() {
    let contents =
    """
    # Heading 1
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [.heading(level: 1, contents: "Heading 1")])
  }

  func test_ParsingSubheading() {
    let contents =
    """
    ## Heading 2
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [.heading(level: 2, contents: "Heading 2")])
  }

  func test_ParsingHeadingsInSeparateParagraphs() {
    let contents =
    """
    # Heading 1

    ## Heading 2
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [
      .heading(level: 1, contents: "Heading 1"),
      .heading(level: 2, contents: "Heading 2")
      ])
  }

  func test_ParsingHeadingsInParagraphsSeparatedByMultipleBlankLines() {
    let contents =
    """
    # Heading 1
    \t    \t


    ## Heading 2
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [
      .heading(level: 1, contents: "Heading 1"),
      .heading(level: 2, contents: "Heading 2")
      ])
  }
}

final class ParserTests_NormalParagraphs: XCTestCase {
  func test_ParsingNormalParagraph() {
    let contents =
    """
    This is a normal paragraph.
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [.normal(contents: [
      .normal(contents: "This is a normal paragraph."),
      ])])
  }

  func test_ParsingHeadingFollowedByNormalParagraph() {
    let contents =
    """
    # Heading 1
    This is a normal paragraph.
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [
      .heading(level: 1, contents: "Heading 1"),
      .normal(contents: [
        .normal(contents: "This is a normal paragraph.")
        ])
      ])
  }
}

final class ParserTests_Code: XCTestCase {
  func test_ParsingCodeParagraph() {
    let contents =
    """
    ```
    let i = 0
    let j = i
    ```
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [
      .code(contents: "let i = 0\nlet j = i")
      ])
  }

  func test_ParsingCodeParagraphWithBlankLines() {
    let contents =
    """
    ```
    let i = 0

    let j = i
    ```
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [
      .code(contents: "let i = 0\n\nlet j = i")
      ])
  }
}

final class MarkdownParserCoreTests: XCTestCase {
  func test_ParsingUnorderedList() {
    let contents =
    """
    - item 1
    - item 2
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [.list(items: [
      [.normal(contents: "item 1")],
      [.normal(contents: "item 2")]
      ])])
  }

  func test_ParsingQuote() {
    let contents =
    """
    > This is a quote.
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [.quote(contents: "This is a quote.")])
  }
}

final class ParserTests_Spans: XCTestCase {
  func test_ParsingLinks_InNormalParagraph() {
    let contents =
    """
    This is a normal paragraph with a [link](http://ya.ru).
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [.normal(contents: [
      .normal(contents: "This is a normal paragraph with a "),
      .link(text: "link", url: URL(string: "http://ya.ru")!),
      .normal(contents: ".")
      ])])
  }

  func test_ParsingItalicText_InNormalParagraph() {
    let contents =
    """
    This is a *normal* paragraph.
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [.normal(contents: [
      .normal(contents: "This is a "),
      .styled(style: .italic, contents: "normal"),
      .normal(contents: " paragraph.")
      ])])
  }

  func test_ParsingInlineCode_InNormalParagraph() {
    let contents =
    """
    This is a `normal` paragraph.
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [.normal(contents: [
      .normal(contents: "This is a "),
      .code(contents: "normal"),
      .normal(contents: " paragraph.")
      ])])
  }

  func test_ParsingInlineCode_InList() {
    let contents =
    """
    - This is `inline code`
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [.list(items: [
      [
        .normal(contents: "This is "),
        .code(contents: "inline code")
      ]
    ])])
  }

  func test_ParsingTwoLinks_InNormalParagraph() {
    let contents =
    """
    This is a [link](http://ya.ru) paragraph [link](http://ya.ru).
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [.normal(contents: [
      .normal(contents: "This is a "),
      .link(text: "link", url: URL(string: "http://ya.ru")!),
      .normal(contents: " paragraph "),
      .link(text: "link", url: URL(string: "http://ya.ru")!),
      .normal(contents: ".")
      ])])
  }

  func test_ParsingTwoInlineCodeBlocks_InNormalParagraph() {
    let contents =
    """
    This is a `normal` `paragraph`.
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [.normal(contents: [
      .normal(contents: "This is a "),
      .code(contents: "normal"),
      .normal(contents: " "),
      .code(contents: "paragraph"),
      .normal(contents: ".")
      ])])
  }

  func test_ParsingItalicText_WhenNormalParagraphStartsWithIt() {
    let contents =
    """
    *This is a normal paragraph:*
    """

    let ps = parseParagraphs(contents)

    XCTAssertEqual(ps, [.normal(contents: [
      .styled(style: .italic, contents: "This is a normal paragraph:")
      ])])
  }
}
