//
//  HTMLRendererTests.swift
//  MarkdownParserCoreTests
//
//  Created by Andrey Mishanin on 24/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import XCTest
import MarkdownRendererCore

private let r = RawHTMLRenderer()
private let indent = 4

final class RawHTMLRendererTests_Headings: XCTestCase {
  func test_RenderingLevel1Heading() {
    let ps: [Paragraph] = [
      .heading(level: 1, contents: "Heading 1"),
    ]

    let expected =
    """
        <h1>Heading 1</h1>
    """
    XCTAssertEqual(r.render(paragraphs: ps, indent: indent), expected)
  }

  func test_RenderingLevel2Heading() {
    let ps: [Paragraph] = [
      .heading(level: 2, contents: "Heading 2"),
      ]

    let expected =
    """
        <h2>Heading 2</h2>
    """
    XCTAssertEqual(r.render(paragraphs: ps, indent: indent), expected)
  }

  func test_RenderingTwoHeadings() {
    let ps: [Paragraph] = [
      .heading(level: 1, contents: "Heading 1"),
      .heading(level: 2, contents: "Heading 2")
    ]

    let expected =
    """
        <h1>Heading 1</h1>

        <h2>Heading 2</h2>
    """
    XCTAssertEqual(r.render(paragraphs: ps, indent: indent), expected)
  }
}

final class RawHTMLRendererTests_NormalParagraph: XCTestCase {
  func test_RenderingNormalParagraph() {
    let ps: [Paragraph] = [.normal(contents: [
      .normal(contents: "This is a normal paragraph."),
      ])]

    let expected =
    """
        <p>This is a normal paragraph.</p>
    """
    XCTAssertEqual(r.render(paragraphs: ps, indent: indent), expected)
  }

  func test_RenderingLinks() {
    let ps: [Paragraph] = [.normal(contents: [
      .normal(contents: "This is a normal paragraph with a "),
      .link(text: "link", url: URL(string: "http://ya.ru")!),
      .normal(contents: ".")
      ])]

    let expected =
    """
        <p>This is a normal paragraph with a <a href="http://ya.ru">link</a>.</p>
    """
    XCTAssertEqual(r.render(paragraphs: ps, indent: indent), expected)
  }

  func test_RenderingItalics() {
    let ps: [Paragraph] = [.normal(contents: [
      .normal(contents: "This is a "),
      .styled(style: .italic, contents: "normal"),
      .normal(contents: " paragraph.")
      ])]

    let expected =
    """
        <p>This is a <em>normal</em> paragraph.</p>
    """
    XCTAssertEqual(r.render(paragraphs: ps, indent: indent), expected)
  }

  func test_RenderingInlineCode() {
    let ps: [Paragraph] = [.normal(contents: [
      .normal(contents: "This is a "),
      .code(contents: "normal"),
      .normal(contents: " paragraph.")
      ])]

    let expected =
    """
        <p>This is a <code>normal</code> paragraph.</p>
    """
    XCTAssertEqual(r.render(paragraphs: ps, indent: indent), expected)
  }
}

final class RawHTMLRendererTests: XCTestCase {
  func test_RenderingList() {
    let ps: [Paragraph] = [.list(items: [
      [
        .normal(contents: "This is "),
        .code(contents: "inline code")
      ]
      ])]

    let expected =
    """
        <ul>
          <li>This is <code>inline code</code></li>
        </ul>
    """
    XCTAssertEqual(r.render(paragraphs: ps, indent: indent), expected)
  }

  func test_RenderingCode() {
    let ps: [Paragraph] = [
      .code(contents: "let i = 0\n\nlet j = i")
    ]

    let expected =
    """
    <pre><code>
    let i = 0

    let j = i
    </code></pre>
    """
    XCTAssertEqual(r.render(paragraphs: ps, indent: indent), expected)
  }

  func test_RenderingQuote() {
    let ps: [Paragraph] = [.quote(contents: "This is a quote.")]

    let expected =
    """
        <blockquote>
          <p>This is a quote.</p>
        </blockquote>
    """
    XCTAssertEqual(r.render(paragraphs: ps, indent: indent), expected)
  }
}
