//
//  FullDocumentRendererTests.swift
//  MarkdownParserCoreTests
//
//  Created by Andrey Mishanin on 25/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import XCTest
import MarkdownRendererCore

final class FullDocumentRendererTests: XCTestCase {
  func test_RenderingFullDocument() {
    let ps: [Paragraph] = [.normal(contents: [
      .normal(contents: "This is a normal paragraph."),
      ])]

    let r = FullDocumentRenderer(title: "This is a title",
                                 stylesheetPath: "Styles/Styles.css",
                                 bodyRenderer: RawHTMLRenderer(),
                                 footer: "<b>This is a footer</b>")

    let expected =
    """
    <!DOCTYPE html>
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="Styles/Styles.css"/>
        <title>This is a title</title>
      </head>
      <body>
        <p>This is a normal paragraph.</p>
        <hr/>
        <footer>
          <b>This is a footer</b>
        </footer>
      </body>
    </html>
    """
    XCTAssertEqual(r.render(paragraphs: ps), expected)
  }
}
