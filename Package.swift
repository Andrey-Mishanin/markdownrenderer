// swift-tools-version:4.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MarkdownRenderer",
    products: [
        .executable(
            name: "MarkdownRenderer",
            targets: ["MarkdownRenderer"]
        ),
        .library(
            name: "MarkdownRendererCore",
            targets: ["MarkdownRendererCore"]
        )
    ],
    targets: [
        .target(
            name: "MarkdownRenderer",
            dependencies: ["MarkdownRendererCore"]),
        .target(
            name: "MarkdownRendererCore",
            dependencies: []),
        .testTarget(
            name: "MarkdownRendererCoreTests", 
            dependencies: ["MarkdownRendererCore"]),
    ]
)
