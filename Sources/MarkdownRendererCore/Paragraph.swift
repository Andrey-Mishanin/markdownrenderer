//
//  Paragraph.swift
//  MarkdownParserCore
//
//  Created by Andrey Mishanin on 12/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

public enum Paragraph {
  case heading(level: Int, contents: String)
  case code(contents: String)
  case normal(contents: Spans)
  case list(items: [Spans])
  case quote(contents: String)
  case blankLine

  var isBlank: Bool {
    if case .blankLine = self { return true } else { return false }
  }
}

extension Paragraph: Equatable {
  public static func ==(lhs: Paragraph, rhs: Paragraph) -> Bool {
    switch (lhs, rhs) {
    case let (.heading(level1, contents1), .heading(level2, contents2)):
      return level1 == level2 && contents1 == contents2
    case let (.normal(contents1), .normal(contents2)):
      return contents1 == contents2
    case let (.code(contents1), .code(contents2)):
      return contents1 == contents2
    case let (.list(items1), .list(items2)):
      return items1 == items2
    case let (.quote(contents1), .quote(contents2)):
      return contents1 == contents2
    case (.blankLine, .blankLine):
      return true
    default:
      return false
    }
  }
}
