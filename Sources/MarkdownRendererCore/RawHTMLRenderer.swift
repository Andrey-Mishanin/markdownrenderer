//
//  HTMLRenderer.swift
//  MarkdownParserCore
//
//  Created by Andrey Mishanin on 24/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

public typealias HTML = String

public protocol HTMLRenderer: class {
  func render(paragraphs: [Paragraph], indent: Int) -> HTML
}

public class RawHTMLRenderer: HTMLRenderer {
  public init() {}

  public func render(paragraphs: [Paragraph], indent: Int = 0) -> HTML {
    return paragraphs.map({ $0.renderAsHTML(indent: indent) }).joined(separator: "\n\n")
  }
}

public class FullDocumentRenderer<Renderer>: HTMLRenderer where Renderer: HTMLRenderer {
  private let title: String
  private let stylesheetPath: String
  private let bodyRenderer: Renderer
  private let footer: HTML

  public init(title: String, stylesheetPath: String, bodyRenderer: Renderer, footer: HTML) {
    self.title = title
    self.stylesheetPath = stylesheetPath
    self.bodyRenderer = bodyRenderer
    self.footer = footer
  }

  public func render(paragraphs: [Paragraph], indent: Int = 0) -> HTML {
    var result = ""
    print("<!DOCTYPE html>", to: &result)
    print("<html>", to: &result)
    print("  <head>", to: &result)
    print("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>", to: &result)
    print("    <link rel=\"stylesheet\" type=\"text/css\" href=\"\(stylesheetPath)\"/>", to: &result)
    print("    <title>\(title)</title>", to: &result)
    print("  </head>", to: &result)
    print("  <body>", to: &result)
    print(bodyRenderer.render(paragraphs: paragraphs, indent: indent + 4), to: &result)
    print("    <hr/>", to: &result)
    print("    <footer>", to: &result)
    print("      \(footer)", to: &result)
    print("    </footer>", to: &result)
    print("  </body>", to: &result)
    print("</html>", terminator: "", to: &result)
    return result
  }
}

protocol HTMLRenderable {
  func renderAsHTML(indent: Int) -> HTML
}

extension Paragraph: HTMLRenderable {
  func renderAsHTML(indent: Int) -> HTML {
    let indentStr = String(repeating: " ", count: indent)
    switch self {
    case let .heading(level, contents):
      let tag = "h\(level)"
      return "\(indentStr)<\(tag)>\(contents)</\(tag)>"
    case let .normal(spans):
      let contents = spans.map({ $0.renderAsHTML(indent: 0) }).joined()
      return "\(indentStr)<p>\(contents)</p>"
    case let .list(items):
      var result = ""
      print("\(indentStr)<ul>", to: &result)
      for i in items {
        let contents = i.map({ $0.renderAsHTML(indent: 0) }).joined()
        print("\(indentStr)  <li>\(contents)</li>", to: &result)
      }
      print("\(indentStr)</ul>", terminator: "", to: &result)
      return result
    case let .code(contents):
      var result = ""
      print("<pre><code>", to: &result)
      print(contents, to: &result)
      print("</code></pre>", terminator: "", to: &result)
      return result
    case let .quote(contents):
      var result = ""
      print("\(indentStr)<blockquote>", to: &result)
      print("\(indentStr)  <p>\(contents)</p>", to: &result)
      print("\(indentStr)</blockquote>", terminator: "", to: &result)
      return result
    default:
      fatalError()
    }
  }
}

extension Span: HTMLRenderable {
  func renderAsHTML(indent _: Int) -> HTML {
    switch self {
    case let .normal(contents):
      return contents
    case let .link(text, url):
      return "<a href=\"\(url)\">\(text)</a>"
    case let .styled(style, contents):
      let tag = style.tag
      return "<\(tag)>\(contents)</\(tag)>"
    case let .code(contents):
      return "<code>\(contents)</code>"
    }
  }
}

extension Span.Style {
  var tag: String {
    switch self {
    case .italic:
      return "em"
    }
  }
}
