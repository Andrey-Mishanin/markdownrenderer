//
//  QuoteParser.swift
//  MarkdownParserCore
//
//  Created by Andrey Mishanin on 22/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

struct QuoteParser: Parser {
  func parse<Lines>(_ lines: Lines) -> (Paragraph, Lines.SubSequence)? where Lines : Collection, Lines.Element : StringProtocol {
    guard let line = lines.first, line.starts(with: ">") else { return nil }
    let contents = String(line.drop(while: { $0 == ">" || $0 == " " }))
    return (.quote(contents: contents), lines.dropFirst())
  }
}
