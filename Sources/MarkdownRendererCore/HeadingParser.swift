//
//  HeadingParser.swift
//  MarkdownParserCore
//
//  Created by Andrey Mishanin on 22/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

struct HeadingParser: Parser {
  func parse<Lines>(_ lines: Lines) -> (Paragraph, Lines.SubSequence)? where Lines : Collection, Lines.Element: StringProtocol {
    guard let line = lines.first, line.starts(with: "#") else { return nil }
    return (makeHeading(line: line), lines.dropFirst())
  }
}

private func makeHeading<S: StringProtocol>(line: S) -> Paragraph {
  let headingMarkers = line.prefix(while: { $0 == "#" })
  let headingContents = line.drop(while: { $0 == "#" || $0 == " " })
  return .heading(level: headingMarkers.count, contents: String(headingContents))
}
