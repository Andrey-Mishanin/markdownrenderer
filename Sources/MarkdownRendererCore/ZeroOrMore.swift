//
//  ZeroOrMore.swift
//  MarkdownParserCore
//
//  Created by Andrey Mishanin on 22/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

struct ZeroOrMore<P>: Parser where P: Parser {
  private let parser: P

  init(_ parser: P) {
    self.parser = parser
  }

  func parse<Lines>(_ lines: Lines) -> ([P.Result], Lines.SubSequence)? where Lines : Collection, Lines.Element : StringProtocol {
    var results: [P.Result] = []
    var currentLines = lines[...]
    while let (r, newLines) = parser.parse(currentLines) {
      results.append(r)
      currentLines = newLines
    }
    return (results, currentLines)
  }
}
