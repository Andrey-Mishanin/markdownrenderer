//
//  Alternative.swift
//  MarkdownParserCore
//
//  Created by Andrey Mishanin on 22/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

struct Alternative<P1, P2>: Parser where P1: Parser, P2: Parser, P1.Result == P2.Result {
  private let parser1: P1
  private let parser2: P2

  init(_ parser1: P1, _ parser2: P2) {
    self.parser1 = parser1
    self.parser2 = parser2
  }

  func parse<Lines>(_ lines: Lines) -> (P1.Result, Lines.SubSequence)? where Lines : Collection, Lines.Element : StringProtocol {
    return parser1.parse(lines) ?? parser2.parse(lines)
  }
}

precedencegroup AlternativeGroup {
  associativity: right
}

infix operator <|>: AlternativeGroup
func <|><P1, P2>(_ parser1: P1, _ parser2: P2) -> Alternative<P1, P2> {
  return Alternative(parser1, parser2)
}
