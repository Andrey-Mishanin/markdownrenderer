//
//  BlankLineParser.swift
//  MarkdownParserCore
//
//  Created by Andrey Mishanin on 22/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

struct BlankLineParser: Parser {
  func parse<Lines>(_ lines: Lines) -> (Paragraph, Lines.SubSequence)? where Lines : Collection, Lines.Element : StringProtocol {
    guard let line = lines.first, isBlankLine(line) else { return nil }
    return (.blankLine, lines.dropFirst())
  }
}

private func isBlankLine<S>(_ line: S) -> Bool where S: Collection, S.Element == Character {
  return line.allSatisfy {
    CharacterSet.whitespaces.contains($0.firstScalar)
  }
}
