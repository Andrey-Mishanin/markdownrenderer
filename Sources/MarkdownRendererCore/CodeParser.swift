//
//  CodeParser.swift
//  MarkdownParserCore
//
//  Created by Andrey Mishanin on 22/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

struct CodeParser: Parser {
  func parse<Lines>(_ lines: Lines) -> (Paragraph, Lines.SubSequence)? where Lines : Collection, Lines.Element : StringProtocol {
    guard let firstLine = lines.first, firstLine.starts(with: "```") else { return nil }
    var currentLines = lines.dropFirst()
    var contents: [String] = []
    while let line = currentLines.first {
      currentLines = currentLines.dropFirst()
      if line != "```" {
        contents.append(String(line))
      } else {
        return (.code(contents: contents.joined(separator: "\n")), currentLines)
      }
    }
    // If we wound up here that means we've never seen the closing ``` marker
    // thus this is not a valid code paragraph
    return nil
  }
}
