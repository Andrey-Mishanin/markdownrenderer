//
//  Parser.swift
//  MarkdownParserCore
//
//  Created by Andrey Mishanin on 12/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

protocol Parser {
  associatedtype Result

  func parse<Lines>(_ lines: Lines) -> (Result, Lines.SubSequence)? where Lines: Collection, Lines.Element: StringProtocol
}

public func parseParagraphs(_ contents: String) -> [Paragraph] {
  let lines = contents.split(omittingEmptySubsequences: false,
                             whereSeparator: {
    return CharacterSet.newlines.contains($0.firstScalar)
  })

  let parser = ZeroOrMore(
    HeadingParser() <|> QuoteParser() <|> ListParser() <|> CodeParser() <|> BlankLineParser() <|> NormalParagraphParser()
  )
  let (p, _) = parser.parse(lines)!

  return p.filter { !$0.isBlank }
}

extension Character {
  var firstScalar: Unicode.Scalar { return unicodeScalars.first! }
}
