//
//  ListParser.swift
//  MarkdownParserCore
//
//  Created by Andrey Mishanin on 22/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

struct ListParser: Parser {
  func parse<Lines>(_ lines: Lines) -> (Paragraph, Lines.SubSequence)? where Lines : Collection, Lines.Element : StringProtocol {
    var currentLines = lines[...]
    var items: [Spans] = []
    while let line = currentLines.first, line.starts(with: "-") {
      currentLines = currentLines.dropFirst()
      let item = String(line.drop(while: { $0 == "-" || $0 == " " }))
      items.append(makeSpans(from: item))
    }
    return items.isEmpty ? nil : (.list(items: items), currentLines)
  }
}
