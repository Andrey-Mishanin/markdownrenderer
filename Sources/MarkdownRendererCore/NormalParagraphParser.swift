//
//  NormalParagraphParser.swift
//  MarkdownParserCore
//
//  Created by Andrey Mishanin on 22/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

struct NormalParagraphParser: Parser {
  func parse<Lines>(_ lines: Lines) -> (Paragraph, Lines.SubSequence)? where Lines : Collection, Lines.Element : StringProtocol {
    guard let line = lines.first else { return nil }
    return (.normal(contents: makeSpans(from: String(line))), lines.dropFirst())
  }
}
