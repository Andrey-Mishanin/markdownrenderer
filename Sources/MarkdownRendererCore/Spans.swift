//
//  Spans.swift
//  MarkdownParserCore
//
//  Created by Andrey Mishanin on 22/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation

public enum Span {
  public enum Style {
    case italic
  }

  case normal(contents: String)
  case code(contents: String)
  case link(text: String, url: URL)
  case styled(style: Style, contents: String)
}

extension Span: Equatable {
  public static func ==(lhs: Span, rhs: Span) -> Bool {
    switch (lhs, rhs) {
    case let (.normal(contents1), .normal(contents2)):
      return contents1 == contents2
    case let (.code(contents1), .code(contents2)):
      return contents1 == contents2
    case let (.link(text1, url1), .link(text2, url2)):
      return text1 == text2 && url1 == url2
    case let (.styled(style1, contents1), .styled(style2, contents2)):
      return style1 == style2 && contents1 == contents2
    default:
      return false
    }
  }
}

public typealias Spans = [Span]

// This function is not generic w.r.t. contents type because regex machinery
// requires Strings
func makeSpans(from contents: String) -> Spans {
  let nsRange = NSRange(contents.startIndex..., in: contents)
  let matches = spanRegex.matches(in: contents, range: nsRange)

  var spans: [Span] = []
  var currentIdx = contents.startIndex
  for m in matches {
    let matchRange = Range(m.range, in: contents)!
    if (currentIdx != matchRange.lowerBound) {
      spans.append(.normal(contents: String(contents[currentIdx..<matchRange.lowerBound])))
    }
    spans.append(makeSpanFrom(match: m, in: contents))
    currentIdx = matchRange.upperBound
  }

  if (currentIdx != contents.endIndex) {
    spans.append(.normal(contents: String(contents[currentIdx...])))
  }

  return spans
}

private func makeSpanFrom(match: NSTextCheckingResult, in contents: String) -> Span {
  let matchingCaptureGroups = (1..<match.numberOfRanges).filter({ match.range(at: $0).location != NSNotFound }).map({ CaptureGroup.init(rawValue: $0)! })

  switch matchingCaptureGroups {
  case [.linkText, .linkURL]:
    let text = String(contents.substringFor(captureGroup: CaptureGroup.linkText, of: match))
    let url = URL(string: String(contents.substringFor(captureGroup: CaptureGroup.linkURL, of: match)))!
    return .link(text: text, url: url)
  case [.inlineCode]:
    let code = String(contents.substringFor(captureGroup: CaptureGroup.inlineCode, of: match))
    return .code(contents: code)
  case [.italic]:
    let text = String(contents.substringFor(captureGroup: CaptureGroup.italic, of: match))
    return .styled(style: .italic, contents: text)
  default:
    fatalError()
  }
}

private extension String {
  func substringFor<CG>(captureGroup: CG, of match: NSTextCheckingResult) -> SubSequence where CG: RawRepresentable, CG.RawValue == Int {
    return self[Range(match.range(at: captureGroup.rawValue), in: self)!]
  }
}

private enum CaptureGroup: Int {
  case linkText = 1
  case linkURL
  case italic
  case inlineCode
}

private let linkRegexPattern = "\\[(.*?)\\]\\((.*?)\\)"
private let italicRegexPattern = "\\*(.*)\\*"
private let codeRegexPattern = "`(.*?)`"
private let spanPatterns = [
  linkRegexPattern,
  italicRegexPattern,
  codeRegexPattern,
]
private let spanRegex = try! NSRegularExpression(pattern: spanPatterns.joined(separator: "|"))
