//
//  main.swift
//  MarkdownParser
//
//  Created by Andrey Mishanin on 12/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation
import MarkdownRendererCore

let inputFilePath = ProcessInfo.processInfo.arguments[1]
let inputFileURL = URL(fileURLWithPath: inputFilePath)
let contents = try! String(contentsOf: inputFileURL)

let ps = parseParagraphs(contents)
let r = FullDocumentRenderer(title: "В защиту «ручного» лэйаута — Set It Off!",
                             stylesheetPath: "Styles/Styles.css",
                             bodyRenderer: RawHTMLRenderer(),
                             footer: "<a href=\"mailto:andrey.mishanin@me.com\">andrey.mishanin@me.com</a> · <a href=\"https://twitter.com/AndreyMishanin\">@AndreyMishanin</a>")
print(r.render(paragraphs: ps))
